<?php

/*
 * Service.php
 * @author Brian
 */
include '../views/header.php';
include '../configs/dbconfig.php';

if(!isset($_SESSION["username"]))
{
    header("Location: ../views/login.php");
    exit();
}

echo "<br>".$_SESSION["userid"]."<br>";
$username = $_SESSION["username"];

$app = new Service($_REQUEST);

/**
 * Description of Service
 *
 * @author Brian
 */
class Service {
    var $regitration;
    var $type;
    var $user;
    var $comments;
    var $service;
    var $petrol;
    var $liters;
    var $mileage;
    var $registered;
    var $trans;
    var $comment;
    var $datep;
    var $vid;
    private $reg;

    function __construct($vehicle) 
    {
        echo "Welcome to: ".COMPANY.".<br>";
        $this->registration = filter_var($vehicle["registration"], FILTER_SANITIZE_STRIPPED);
        $this->type = filter_var($vehicle["ttype"],FILTER_SANITIZE_STRIPPED);
        $this->user = filter_var($vehicle["name"], FILTER_SANITIZE_STRIPPED);
        $this->comments = filter_var($vehicle["comments"], FILTER_SANITIZE_STRIPPED);
        $this->mileage = filter_var($vehicle["mileage"], FILTER_SANITIZE_STRIPPED);
        $this->liters = filter_var($vehicle["liter"], FILTER_SANITIZE_STRIPPED);
        $this->trans = 0;
        $this->comment = 'New service or fill up.';
        $this->registered = false;
        $this->service = 0;
        $this->petrol = 0;
        $this->datep = time();
        $this->reg = filter_var($vehicle["registration"], FILTER_SANITIZE_STRIPPED);
        $this->process();
        $this->validate();
    }
    
    function validate()
    {
        echo "$this->reg <br>";
        echo "$this->user <br>";
        echo "$this->datep <br>";
        echo "$this->type <br>";
        echo "$this->mileage <br>";
        echo "$this->liters <br>";
        if($this->registered)
        {
            echo '<tr><td><a href="../views/welcome.php">Let\'s go driving</a></td></tr>';
            $this->newServicefb();
        }
    }
    
     private function newServicefb(){
        try{
            include_once '../firebasep/Servicefb.php';
            $fbdb = new Servicefb();
            $fbdb->newServicefb($this->reg, $this->type, $this->user, $this->mileage, $this->liters, $this->comments);
        } catch (Exception $ex) {
            echo '<tr><td>'.$ex->getMessage().'</td></tr>';
        }
    }
    
    function process()
    {
        $temp3 = $this->registration;
        echo "$this->type <br>";
        if($this->type == "service"){
            $this->service = true;
        }else {
            $this->petrol = true;
        }
        if($temp3 == "")
        {
            $msg = "Erro: regitration not valid! <br>";
            header("Location: ../views/vservice.php?message=$msg");
            exit();
        }
        $this->findx($temp3);
        $this->registerp();
        $this->updateMileage();
    }
    
    function registerp()
    {
        try 
        {
            echo " process vservice <br>";
            echo "$this->regitration <br>";
            include_once '../configs/dbconn.php';
            $mysqli = connDB();
            echo "$mysqli->host_info <br>";
            echo "$mysqli->server_info <br>";
            $datepp = date("Y/m/d H:i:s", $this->datep);
            $reg = $this->registration;
            $mile = $this->mileage;
            echo "$datepp <br>";
            $query = "INSERT INTO service(registration, user, date, service, petrol, liter, mileage, comment)"
                     ."VALUES(?,?,?,?,?,?,?,?)";
            $smt = $mysqli->prepare($query);
            $smt->bind_param("ssssssss", $reg, $this->user, $datepp, 
                    $this->service, $this->petrol, $this->liters, $mile, $this->comments);
            $this->registered = $smt->execute();
            $smt->close();
            $mysqli->close();
        } 
        catch (Exception $ex) 
        {
            echo "<br> EXCE:.......... <br>";
            echo "$ex->getMessage()";            
        }
        echo '<tr><td><a href="../views/welcome.php">Exit2</a></td></tr>';
    }
    
    function findx($xuser)
    {
        echo "$xuser <br>";
        try 
        {
            $useri = "";
            $usern = "";
            include_once '../configs/dbconn.php';
            $mysqli = connDB();
            $query = "SELECT id, make FROM vehicle WHERE registration = ?";
            $smt = $mysqli->prepare($query);
            $smt->bind_param("s", $xuser);
            $info = $smt->execute();
            $resultb = $smt->bind_result($useri, $usern);
            $fetch = $smt->fetch();
            $smt->close();
            $mysqli->close();
            $findp[] = $info;
            $findp[] = $resultb;
            $findp[] = $fetch;
            echo "01exec: $findp[0] <br>";
            echo "12reslt: $findp[1] <br>";
            echo "usrid: $useri <br>";
            echo "name: $usern <br>";
            echo "fetch: $findp[2] <br>";
            $this->vid = $useri;
            if($useri == "")
            {
                echo "$this->comment <br>";
                $this->trans = 1;
                echo '<tr><td><a href="../views/vtrip.php">Exit1</a></td></tr>';
                header("Location: ../views/vtrip.php");
                exit();  
            }
            else if($this->trans === 0)
            {
                echo " $xuser <br>";
                echo '<tr><td><a href="../views/welcome.php">Exit01</a></td></tr>';                  
            }
        } 
        catch (Exception $ex) 
        {
            echo "<br> EXCEPT:....... <br>";
            echo "$ex->getMessage()";            
        } 
    }
    
    function updateMileage()
    {
        try 
        {
            echo "$this->regitration <br>";
            include_once '../configs/dbconn.php';
            $mysqli = connDB();
            echo "$mysqli->host_info <br>";
            echo "$mysqli->server_info <br>";
            $datepp = date("Y/m/d H:i:s", $this->datep);
            $reg = $this->registration;
            $mile = $this->mileage;
            echo "$mile <br>";
            echo "$reg <br>";
            echo "$this->vid <br>";
            echo "$datepp <br>";
            $query = "UPDATE vehicle SET mileage = ? WHERE registration = ?";
            //
            $smt = $mysqli->prepare($query);
            $smt->bind_param('ss',$mile, $reg);
            $ext = $smt->execute();
            //
            echo "$ext <br>";
            $smt->close();
            $mysqli->close();
        } 
        catch (Exception $ex) 
        {
            echo "<br> EXCE:.......... <br>";
            echo "$ex->getMessage()";            
        }
    }
}

?>

<?php
echo "<br><br>";
include "../views/footer.php";
?>
