<?php

/*
 * Trip.php
 * @author Brian
 */
include '../views/header.php';
include '../configs/dbconfig.php';

if(!isset($_SESSION["username"]))
{
    header("Location: ../views/login.php");
    exit();
}

echo "<br>".$_SESSION["userid"]."<br>";
$username = $_SESSION["username"];

$app = new Trip($_REQUEST);

/**
 * Description of Trip
 *
 * @author Brian
 */
class Trip {
    var $registration;
    var $type;
    var $user;
    var $comments;
    var $business;
    var $pesornal;
    var $registered;
    var $trans;
    var $comment;
    var $datep;
    var $reg;
    
    function __construct($vehicle) 
    {
        echo "Welcome to: ".COMPANY.".<br>";
        $this->registration = filter_var($vehicle["registration"], FILTER_SANITIZE_STRIPPED);
        $this->type = filter_var($vehicle["ttype"],FILTER_SANITIZE_STRIPPED);
        $this->user = filter_var($vehicle["name"], FILTER_SANITIZE_STRIPPED);
        $this->comments = filter_var($vehicle["comments"], FILTER_SANITIZE_STRIPPED);
        $this->trans = 0;
        $this->comment = 'New trip.';
        $this->registered = false;
        $this->business = 0;
        $this->pesornal = 0;
        $this->datep = time();
        $this->process();
        $this->validate();
    }
    
    function validate()
    {
        echo "1 $this->registration <br>";
        echo "2 $this->user <br>";
        echo "3 $this->datep <br>";
        echo "4 $this->comments <br>";
        if($this->registered)
        {
            echo '<tr><td><a href="../views/welcome.php">Let\'s go driving</a></td></tr>';
            $this->newTripfb();
        }
    }
    
    private function newTripfb(){
        try{
            include_once '../firebasep/Tripfb.php';
            $fbdb = new Tripfb();
            $fbdb->newTripfb($this->registration, $this->type, $this->user, $this->comments);
        } catch (Exception $ex) {
            echo '<tr><td>'.$ex->getMessage().'</td></tr>';
        }
    }
    
    function process()
    {
        $temp3 = $this->registration;
        echo "$this->type <br>";
        if($this->type == "personal"){
            $this->pesornal = true;
        }else {
            $this->business = true;
        }
        if($temp3 == "")
        {
            $msg = "Erro: regitration not valid! <br>";
            header("Location: ../views/vtrip.php?message=$msg");
            exit();
        }
        $this->findx($temp3);
        $this->registerp();
    }
    
    function registerp()
    {
        try 
        {
            echo "p1 process vtrip <br>";
            echo "p2 $this->registration <br>";
            include_once '../configs/dbconn.php';
            $mysqli = connDB();
            echo "$mysqli->host_info <br>";
            echo "$mysqli->server_info <br>";
            $datepp = date("Y/m/d H:i:s", $this->datep);
            echo "$datepp <br>";
            $query = "INSERT INTO trip(registration, user, date, business, personal, comment)"
                     ."VALUES(?,?,?,?,?,?)";
            $smt = $mysqli->prepare($query);
            $smt->bind_param("ssssss", $this->registration, $this->user, $datepp, 
                    $this->business, $this->pesornal, $this->comments);
            $this->registered = $smt->execute();
            $smt->close();
            $mysqli->close();
        } 
        catch (Exception $ex) 
        {
            echo "<br> EXCE:.......... <br>";
            echo "$ex->getMessage()";            
        }
        echo '<tr><td><a href="../views/welcome.php">Exit2</a></td></tr>';
    }
    
    function findx($xuser)
    {
        echo "$xuser <br>";
        try 
        {
            $useri = "";
            $usern = "";
            include_once '../configs/dbconn.php';
            $mysqli = connDB();
            $query = "SELECT id, make FROM vehicle WHERE registration = ?";
            $smt = $mysqli->prepare($query);
            $smt->bind_param("s", $xuser);
            $info = $smt->execute();
            $resultb = $smt->bind_result($useri, $usern);
            $fetch = $smt->fetch();
            $smt->close();
            $mysqli->close();
            $findp[] = $info;
            $findp[] = $resultb;
            $findp[] = $fetch;
            echo "01exec: $findp[0] <br>";
            echo "12reslt: $findp[1] <br>";
            echo "usrid: $useri <br>";
            echo "name: $usern <br>";
            echo "fetch: $findp[2] <br>";
            if($useri == "")
            {
                echo "$this->comment <br>";
                $this->trans = 1;
                echo '<tr><td><a href="../views/vtrip.php">Exit1</a></td></tr>';
                header("Location: ../views/vtrip.php");
                exit();  
            }
            else if($this->trans === 0)
            {
                echo " $xuser <br>";
                echo '<tr><td><a href="../views/welcome.php">Exit01</a></td></tr>';                  
            }
        } 
        catch (Exception $ex) 
        {
            echo "<br> EXCEPT:....... <br>";
            echo "$ex->getMessage()";            
        } 
    }   
}
?>

<?php
echo "<br><br>";
include "../views/footer.php";
?>
