<?php

/*
 * Vehicle.php
 * @author Brian
 */
include '../views/header.php';
include '../configs/dbconfig.php';

if(!isset($_SESSION["username"]))
{
    header("Location: ../views/login.php");
    exit();
}

$app = new Vehicle($_REQUEST);

/**
 * Description of Vehicle
 *
 * @author Brian
 */
class Vehicle {
    var $regitration;
    var $make;
    var $model;
    var $mileage;
    var $registered;
    var $trans;
    var $comment;
    var $datep;
    private $reg;

    function __construct($vehicle) 
    {
        echo "Welcome to: ".COMPANY.".<br>";
        $this->registration = filter_var($vehicle["registration"], FILTER_SANITIZE_STRIPPED);
        $this->make = filter_var($vehicle["make"],FILTER_SANITIZE_STRIPPED);
        $this->model = filter_var($vehicle["model"], FILTER_SANITIZE_STRIPPED);
        $this->mileage = filter_var($vehicle["mileage"], FILTER_SANITIZE_STRIPPED);
        $this->trans = 0;
        $this->comment = 'New vehicle.';
        $this->registered = false;
        $this->datep = time();
        $this->reg = filter_var($vehicle["registration"], FILTER_SANITIZE_STRIPPED);
        $this->process();
        $this->validate();
        $this->findx($this->registration);
    }
    
    function validate()
    {
        echo "$this->reg <br>";
        echo "$this->make <br>";
        echo "$this->model <br>";
        echo "$this->mileage <br>";
        if($this->registered)
        {
            echo '<tr><td><a href="../views/welcome.php">Let\'s go driving</a></td></tr>';
            $this->newVfb();
        }
    }
    
    private function newVfb(){
        try{
            include_once '../firebasep/Vehiclefb.php';
            $fbdb = new Vehiclefb();
            $fbdb->newVehiclefb($this->reg, $this->make, $this->model, $this->mileage);
        } catch (Exception $ex) {
            echo '<tr><td>'.$ex->getMessage().'</td></tr>';
        }
    }
    
    function process()
    {
        $temp3 = $this->registration;
        echo "$temp3 <br>";
        if($temp3 == "")
        {
            $msg = "Erro: regitration not valid! <br>";
            header("Location: ../views/vregistration.php?message=$msg");
            exit();
        }
        $this->findx($temp3);
        $this->registerp();
    }
    
    function registerp()
    {
        try 
        {
            echo "$this->regitration <br>";
            include_once '../configs/dbconn.php';
            $mysqli = connDB();
            echo "$mysqli->host_info <br>";
            echo "$mysqli->server_info <br>";
            $datepp = date("Y/m/d H:i:s", $this->datep);
            echo "$datepp <br>";
            $query = "INSERT INTO vehicle(registration,make,model,mileage)"
                     ."VALUES(?,?,?,?)";
            $smt = $mysqli->prepare($query);
            $smt->bind_param("ssss", $this->registration, $this->make, 
                    $this->model, $this->mileage);
            $this->registered = $smt->execute();
            $smt->close();
            $mysqli->close();
        } 
        catch (Exception $ex) 
        {
            echo "<br> EXCE:.......... <br>";
            echo "$ex->getMessage()";            
        }
    }
    
    function findx($xuser)
    {
        echo "$xuser <br>";
        $this->registration = $xuser;
        try 
        {
            $useri = "";
            $usern = "";
            include_once '../configs/dbconn.php';
            $mysqli = connDB();
            $query = "SELECT id, make FROM vehicle WHERE registration = ?";
            $smt = $mysqli->prepare($query);
            $smt->bind_param("s", $this->registration);
            $info = $smt->execute();
            $resultb = $smt->bind_result($useri, $usern);
            $fetch = $smt->fetch();
            $smt->close();
            $mysqli->close();
            $findp[] = $info;
            $findp[] = $resultb;
            $findp[] = $fetch;
            echo "01exec: $findp[0] <br>";
            echo "12reslt: $findp[1] <br>";
            echo "usrid: $useri <br>";
            echo "name: $usern <br>";
            echo "fetch: $findp[2] <br>";
            if($useri == "")
            {
                echo "$this->comment <br>";
                $this->trans = 1;
            }
            else if($this->trans === 0)
            {
                echo " $this->regitration is already registered <br>";
                echo '<tr><td><a href="../welcome.php">Exit</a></td></tr>';
                header("Location: ../views/welcome.php");
                exit();                    
            }
        } 
        catch (Exception $ex) 
        {
            echo "<br> EXCEPT:....... <br>";
            echo "$ex->getMessage()";            
        } 
    }   
}

?>

<?php
echo "<br><br>";
include "../views/footer.php";
?>
