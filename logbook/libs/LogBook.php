<?php
/*
 * LogBook.php
 * @author Brian
 */
include '../views/header.php';
include '../configs/dbconfig.php';

$app = new LogBook($_REQUEST);

/**
 * Description of LogBook
 *
 * @author Brian
 */
class LogBook 
{
    var $name;
    var $email;
    var $password;
    var $datep;
    var $comments;
    var $userid;
    var $userpwd;
    var $fb;
    
    function __construct($userlog) 
    {
        $this->email = filter_var($userlog["email"],FILTER_VALIDATE_EMAIL);
        $this->password = filter_var($userlog["password"], FILTER_SANITIZE_STRIPPED);
        $this->fb = 0;
        $this->process(); 
    }
    
    function process ()
    {
        $temp3 = $this->email;
        if ($temp3 == "")
        {
            header("Location: ../views/login.php");
            exit();
        }
        $this->loginFB();
        $temp = "temp1@";
        $temp .= $this->password;
        $temp .= $this->email[0];
        $temp .= COMPANY[6];
        $temp2 = hash('sha256', $temp);
        $this->password = $temp2;
        $this->user = $temp3;
        $this->login();
    }
    
    function loginFB(){
        try {
            include_once '../firebasep/Usersfb.php';
            $fbdb = new Usersfb();
            $fbdb->signIn($this->email, $this->password);
            $username = $fbdb->getUsername();
            $userid = $fbdb->getUid();
            echo '<tr><td> UIDfb: '.$userid.'</td></tr>';
            echo '<tr><td> USERfb: '.$username.'</td></tr>';
            $_SESSION['useridfb'] = $userid;
            if($username === "0")
            {
                echo '<tr><td> USERfb0: '.$username.'</td></tr>';
            } else {
                $_SESSION['userid'] = $userid;
                $_SESSION['username'] = $username;
                $this->fb = 1;
            }
        } catch (Exception $ex) {
            echo 'FBExc '.$ex->getMessage().'<br>';
        }
    }
            
    function login()
    {
        try 
        {
            include '../configs/dbconn.php';
            $mysqli = connDB();
            $query = "SELECT id, password FROM user WHERE email = ?";
            $smt = $mysqli->prepare($query);
            $smt->bind_param("s", $this->user);
            $info = $smt->execute();
            $smt->bind_result($this->userid, $this->userpwd);
            $fetch = $smt->fetch();
            $smt->close();
            $mysqli->close();
            
            if($this->userpwd == $this->password)
            {
                echo "userid: $this->userid <br>";
                echo "username: $this->user <br>";
                $_SESSION['userid'] = $this->userid;
                $_SESSION['username'] = $this->email;
                $_SESSION['login3'] = "true3".$this->userid;
                echo "".$_SESSION['userid']."<br>";
                echo "".$_SESSION['login3']."<br>";
                header("Location: ../views/welcome.php");
                exit();
            }
            else 
            {
                echo "A: $info <br>";
                echo "B: $fetch <br>";
                if($this->fb === 0)
                {
                    header("Location: ../views/login.php");
                    exit();
                }
            }   
        } 
        catch (Exception $ex) 
        {
            echo "<br> EXCEPT:....... <br>";
            echo "$ex->getMessage()";            
        }   
    }
}
?>

<?php
echo "<br><br>";
include "../views/footer.php";
?>