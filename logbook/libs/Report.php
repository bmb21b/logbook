<?php

/*
 * Report.php
 * @author Brian
 */
include '../views/header.php';
include '../configs/dbconfig.php';

if(!isset($_SESSION["username"]))
{
    header("Location: ../views/login.php");
    exit();
}

echo "<table><tr><td>".$_SESSION["userid"]."</td>";
echo "<td>".$_SESSION["useridfb"]."</td>";
echo "<td><a href=\"../views/welcome.php\">Home</a></td>";
echo "<td><a href=\"../index.php\">Log Out</a></td></tr></table>";

$username = $_SESSION["username"];
echo '<hr>';
$app = new Report();

/**
 * Description of Report
 *
 * @author Brian
 */
class Report {

    function __construct() 
    {
        echo "Welcome to: ".COMPANY.".<br>";
        echo '<br><br></td></tr></table><br>';
        $this->printUsers();
        $this->printVehicles();
        $this->printTrip();
        $this->printService();
        echo '<table width=600px align="center" valign="top"><tr><td heigth="100%">';
        
    }
    
    function printUsers(){
        $query = "SELECT * FROM user";
        echo '<table cellpadding=\"3\" cellspacing=\"1\" border=\"1\"><caption>'.$query.'</caption>';
        echo '<tr><th>ID</th><th>Name</th><th>Email</th><th>HashCode</th><th>Date</th></tr>';
        $this->infoX($query);
    }
    
    function printVehicles(){
        $query = "SELECT * FROM vehicle";
        echo '<table cellpadding=\"3\" cellspacing=\"1\" border=\"1\"><caption>'.$query.'</caption>';
        echo '<tr><th>ID</th><th>Registration</th><th>Make</th><th>Model</th><th>Mileage</th></tr>';
        $this->infoX($query);
    }
    
    function printTrip(){
        $query = "SELECT * FROM trip";
        echo '<table cellpadding=\"3\" cellspacing=\"1\" border=\"1\"><caption>'.$query.'</caption>';
        echo '<tr><th>ID</th><th>Registration</th><th>User</th><th>Date</th><th>Business</th><th>Personal</th><th>Comment</th></tr>';
        $this->infoX($query);
    }
    
    function printService(){
        $query = "SELECT * FROM service";
        echo '<table cellpadding=\"3\" cellspacing=\"1\" border=\"1\"><caption>'.$query.'</caption>';
        echo '<tr><th>ID</th><th>Registration</th><th>User</th><th>Date</th><th>Service</th><th>Petrol</th><th>Liter</th><th>Mileage</th><th>Comment</th></tr>';
        $this->infoX($query);
    }
    
    function infoX($query)
    {
        try 
        {
            include_once '../configs/dbconn.php';
            $mysqli = connDB();
            $smt = $mysqli->prepare($query);
            $smt->execute();
            $rs = $smt->get_result();
            $fc = $rs->field_count;
            $nc = $rs->num_rows;
            for ($j = 0; $j < $nc; $j++ )
            {
                echo '<tr>';
                $rsa = $rs->fetch_array();
                for ($i = 0; $i < $fc; $i++)
                {
                    echo '<td>'.$rsa[$i].'</td>';
                }
                echo '</tr>';
            }
            echo '</table><br>';
            $smt->close();
            $mysqli->close();
        } 
        catch (Exception $ex) 
        {
            echo "<br> EXCEPT:....... <br>";
            echo "$ex->getMessage()";            
        } 
    }   
}

?>

<?php
echo "<br><br>";
include "../views/footer.php";
?>