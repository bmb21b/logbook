<?php

/*
 * Users.php
 * @author Brian
 */
include '../views/header.php';
include '../configs/dbconfig.php';

$app = new Users($_REQUEST);

/**
 * Description of Users
 *
 * @author Brian
 */
class Users 
{
    var $name;
    var $email;
    var $password;
    var $datep;
    var $registered;
    var $trans;
    var $comment;
    
    function __construct($user) 
    {
        echo "Welcome to: ".COMPANY.".<br>";
        $this->name = filter_var($user["name"], FILTER_SANITIZE_STRIPPED);
        $this->email = filter_var($user["email"],FILTER_VALIDATE_EMAIL);
        $this->password = filter_var($user["password"], FILTER_SANITIZE_STRIPPED);
        $this->trans = 0;
        $this->comment = 'New account.';
        $this->registered = false;
        $this->datep = time();
        $this->process();
        $this->validate();
        $this->findx($this->email);
    }
    
   private function firebaseDb($name, $email, $passwd){
        try{
            include_once '../firebasep/Usersfb.php';
            $fbdb = new Usersfb();
            $fbdb->newUser($name, $email, $passwd);
            $username = $fbdb->getUsername();
            $userid = $fbdb->getUid();
            echo '<tr><td> UIDfb: '.$userid.'</td></tr>';
            if(($userid != 0) && ($username != "0"))
            {
                echo '<tr><td><a href="../views/login.php">Let\'s go driving FB</a></td></tr>';
            }
        } catch (Exception $ex) {
            echo '<tr><td>'.$ex->getMessage().'</td></tr>';
        }
    }
            
    function validate()
    {
        echo "$this->name <br>";
        echo "$this->email <br>";
        echo "$this->password <br>";
        echo "$this->registered <br>";
        if($this->registered)
        {
            echo '<tr><td><a href="../views/login.php">Let\'s go driving MySQL</a></td></tr>';
        }
    }
    
    function process()
    {
        $temp3 = $this->email;
        if($temp3 == "")
        {
            $msg = "Erro: email not valid! <br>";
            header("Location: ../views/register.php?message=$msg");
            exit();
        }
        $this->findx($temp3);
        $this->firebaseDb($this->name, $this->email, $this->password);
        echo '<tr><td> USERMySQL: '.$temp3.'</td></tr>';
        $temp = "temp1@";
        $temp .= $this->password;
        $temp .= $this->email[0];
        $temp .= COMPANY[6];
        $temp2 = hash('sha256', $temp);
        $this->password = $temp2;
        $this->registerp();
    }
    
    function registerp()
    {
        try 
        {
            include_once '../configs/dbconn.php';
            $mysqli = connDB();
            echo '<tr><td> host: '.$mysqli->host_info.'</td></tr>';
            echo '<tr><td> Conn#: '.$mysqli->server_info.'</td></tr>';
            $datepp = date("Y/m/d H:i:s", $this->datep);
            echo '<tr><td> Conn#: '.$datepp.'</td></tr>';
            $query = "INSERT INTO user(name,email,password,date)"
                     ."VALUES(?,?,?,?)";
            $smt = $mysqli->prepare($query);
            $smt->bind_param("ssss", $this->name, $this->email, 
                    $this->password, $datepp);
            $this->registered = $smt->execute();
            $smt->close();
            $mysqli->close();
        } 
        catch (Exception $ex) 
        {
            echo '<tr><td>'.$ex->getMessage().'</td></tr>';            
        }
    }
    
    function findx($xuser)
    {
        $this->user = $xuser;
        try 
        {
            $useri = "";
            $usern = "";
            include_once '../configs/dbconn.php';
            $mysqli = connDB();
            $query = "SELECT id, name FROM user WHERE email = ?";
            $smt = $mysqli->prepare($query);
            $smt->bind_param("s", $this->user);
            $info = $smt->execute();
            $resultb = $smt->bind_result($useri, $usern);
            $fetch = $smt->fetch();
            $smt->close();
            $mysqli->close();
            $findp[] = $info;
            $findp[] = $resultb;
            $findp[] = $fetch;
            echo "01exec: $findp[0] <br>";
            echo "12reslt: $findp[1] <br>";
            echo "usrid: $useri <br>";
            echo "usrname: $usern <br>";
            echo "fetch: $findp[2] <br>";
            if($useri == "")
            {
                echo "$this->comment <br>";
                $this->trans = 1;
            }
            else if($this->trans === 0)
            {
                echo " $this->user is already registered <br>";
                echo '<tr><td><a href="../index.php">Exit</a></td></tr>';
                header("Location: ../views/login.php");
                exit();                    
            }
        } 
        catch (Exception $ex) 
        {
            echo '<tr><td>'.$ex->getMessage().'</td></tr>';
        } 
    }
}
?>

<?php
echo "<br><br>";
include "../views/footer.php";
?>