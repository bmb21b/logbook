<?php

/*
 * Vehiclefb.php
 */
if(!isset($_SESSION["username"]))
{
    echo '<tr><td>Vehiclefb offline.</td></tr>';
    echo '<br><a href="../index.php">Exit</a><hr><br>';
    exit();
}
require '../vendor/autoload.php';

use Kreait\Firebase;
use Kreait\Firebase\Auth;
use Kreait\Firebase\Database;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

/**
 * Description of Vehiclefb
 *
 * @author Brian
 */
class Vehiclefb {
    var $registration;
    var $make;
    var $model;
    var $mileage;
    var $trans;
    var $comment;
    var $datep;
    
    function __construct() 
    {
        $this->registration = "0";
        $this->make = "0";
        $this->model = "0";
        $this->mileage = "0";
        $this->trans = 0;
        $this->comment = 'New vehicle.';
        $this->registered = false;
        $this->datep = time();
    }
    
    public function newVehiclefb($reg, $mak, $mod, $mil){
        $this->registration = filter_var($reg, FILTER_SANITIZE_STRIPPED);
        $this->make = filter_var($mak,FILTER_SANITIZE_STRIPPED);
        $this->model = filter_var($mod, FILTER_SANITIZE_STRIPPED);
        $this->mileage = filter_var($mil, FILTER_SANITIZE_STRIPPED);
        $this->newVehicle();
    }
    
    private function newVehicle(){
        try {
            echo '<tr><td>Reg# '.$this->registration.'</td></tr>';
            //$uri = "https://logbook-5a408.firebaseio.com";
            //$firebase = (new Factory)->withDatabaseUri($uri)->create();
            $sac = ServiceAccount::fromJsonFile(__DIR__.'/logbook-sac.json');
            $firebase = (new Factory)->withServiceAccount($sac)->create();
            
            $database = $firebase->getDatabase();
            $newV = $database->getReference('vehicle')->push([
                "id" => $this->datep,
                "mak" => $this->make,
                "mil" => $this->mileage,
                "mod" => $this->model,
                "reg" => $this->registration
            ]); 
            $this->trans = $newV->getKey();
        } catch (Exception $ex) {
            echo '<tr><td>1 '.$ex->getMessage().'</td></tr>';
        } catch (ErrorException $cx){
            echo '<tr><td>2 '.$cx->getMessage().'</td></tr>';
        } catch (Error $err){
            echo '<tr><td>3 '.$err->getMessage().'</td></tr>';
        }
        echo '<tr><td>'.$this->trans.'</td></tr>';
        echo '<tr><td>'.$this->comment.'</td></tr>';
        echo '<tr><td>'.$this->datep.'</td></tr>';
        echo '<tr><td><a href="../index.php">ExitFB1</a></td></tr>';
    }
}
?>