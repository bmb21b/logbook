<?php

/*
 * Tripfb.php
 */
if(!isset($_SESSION["username"]))
{
    echo '<tr><td>Tripfb offline.</td></tr>';
    echo '<br><a href="../index.php">Exit</a><hr><br>';
    exit();
}
require '../vendor/autoload.php';

use Kreait\Firebase;
use Kreait\Firebase\Auth;
use Kreait\Firebase\Database;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

/**
 * Description of Tripfb
 *
 * @author Brian
 */
class Tripfb {
    var $registration;
    var $type;
    var $user;
    var $comments;
    var $trans;
    var $comment;
    var $datep;
    
    function __construct() 
    {
        $this->registration = "0";
        $this->type = "0";
        $this->user = "0";
        $this->comments = "0";
        $this->trans = 0;
        $this->comment = 'New trip.';
        $this->datep = time();
    }
    
    public function newTripfb($reg, $typ, $nam, $com){
        $this->registration = filter_var($reg, FILTER_SANITIZE_STRIPPED);
        $this->type = filter_var($typ,FILTER_SANITIZE_STRIPPED);
        $this->user = filter_var($nam, FILTER_SANITIZE_STRIPPED);
        $this->comments = filter_var($com, FILTER_SANITIZE_STRIPPED);
        $this->newTrip();
    }
    
    private function newTrip(){
        try {
            echo '<tr><td>Reg# '.$this->registration.'</td></tr>';
            //$uri = "https://logbook-5a408.firebaseio.com";
            //$firebase = (new Factory)->withDatabaseUri($uri)->create();
            $sac = ServiceAccount::fromJsonFile(__DIR__.'/logbook-sac.json');
            $firebase = (new Factory)->withServiceAccount($sac)->create();
            
            $database = $firebase->getDatabase();
            $datepp = date("Y/m/d H:i:s", time());
            $newT = $database->getReference('trip')->push([
                "dte" => $datepp,
                "id" => $this->datep,
                "reg" => $this->registration,
                "typ" => $this->type,
                "usr" => $this->user
            ]); 
            $this->trans = $newT->getKey();
        } catch (Exception $ex) {
            echo '<tr><td>1 '.$ex->getMessage().'</td></tr>';
        } catch (ErrorException $cx){
            echo '<tr><td>2 '.$cx->getMessage().'</td></tr>';
        } catch (Error $err){
            echo '<tr><td>3 '.$err->getMessage().'</td></tr>';
        }
        echo '<tr><td>'.$this->trans.'</td></tr>';
        echo '<tr><td>'.$this->comment.'</td></tr>';
        echo '<tr><td>'.$this->datep.'</td></tr>';
        echo '<tr><td><a href="../index.php">ExitFB1</a></td></tr>';
    }
}
?>