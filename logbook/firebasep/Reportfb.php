<?php

/*
 * Reportfb.php
 */
session_start();
if(!isset($_SESSION["username"]))
{
    echo '<tr><td>Tripfb offline.</td></tr>';
    echo '<br><a href="../index.php">Exit</a><hr><br>';
    exit();
}

require '../vendor/autoload.php';
use Kreait\Firebase;
use Kreait\Firebase\Auth;
use Kreait\Firebase\Database;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

$app = new Reportfb();

/**
 * Description of Reportfb
 *
 * @author Brian
 */
class Reportfb {
    
    function __construct() {
        $this->report();
    }
    
    private function report(){
        try {
            //$uri = "https://logbook-5a408.firebaseio.com";
            //$firebase = (new Factory)->withDatabaseUri($uri)->create();
            $sac = ServiceAccount::fromJsonFile(__DIR__.'/logbook-sac.json');
            $firebase = (new Factory)->withServiceAccount($sac)->create();
            
            $database = $firebase->getDatabase();
            $rep = $database->getReference()->getSnapshot()->getValue();
            echo '<br><a href="../index.php">ExitFBR1</a><hr><br>';
            $len = count($rep);
            echo $len, ' <br>';
            echo json_encode($rep, JSON_PRETTY_PRINT);
            echo '<hr>';
            echo "<H1> DB Array Size: ",$len,"</H1><hr>";
            foreach ($rep as $key1 => $v1){
                echo $key1, "<br>";
                foreach ($v1 as $key2 => $value2) {
                    echo $key2, "<br>";
                    foreach ($value2 as $key3 => $v3){
                        echo $key3, " => ", $v3, "<br>";
                    }
                }
                echo '<hr>';
            }
            echo '<br>';
        } catch (Exception $ex) {
            echo $ex->getMessage();
            echo '<br><a href="../index.php">ExitFBR2</a>';
        }catch (ErrorException $cx){
            echo '<tr><td>2 '.$cx->getMessage().'</td></tr>';
            echo '<br><a href="../index.php">ExitFBR3</a>';
        } catch (Error $err){
            echo '<tr><td>3 '.$err->getMessage().'</td></tr>';
            echo '<br><a href="../index.php">ExitFBR4</a>';
        } 
    }   
}

?>