<?php

/*
 * Servicefb.php
 */
if(!isset($_SESSION["username"]))
{
    echo '<tr><td>Servicefb offline.</td></tr>';
    echo '<br><a href="../index.php">Exit</a><hr><br>';
    exit();
}
require '../vendor/autoload.php';

use Kreait\Firebase;
use Kreait\Firebase\Auth;
use Kreait\Firebase\Database;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

/**
 * Description of Servicefb
 *
 * @author Brian3
 */
class Servicefb {
    var $registration;
    var $type;
    var $user;
    var $comments;
    var $service;
    var $petrol;
    var $liters;
    var $mileage;
    var $trans;
    var $comment;
    var $datep;
    var $vid;
    
    function __construct() 
    {
        $this->registration = "0";
        $this->type = "0";
        $this->user = "0";
        $this->comments = "0";
        $this->mileage = 0;
        $this->liters = 0;
        $this->trans = 0;
        $this->comment = "0";
        $this->service = 0;
        $this->petrol = 0;
        $this->datep = time();
        $this->vid = "0";
    }
    
    public function newServicefb($reg, $typ, $nam, $mil, $ltr, $com){
        $this->registration = filter_var($reg, FILTER_SANITIZE_STRIPPED);
        $this->type = filter_var($typ,FILTER_SANITIZE_STRIPPED);
        $this->user = filter_var($nam, FILTER_SANITIZE_STRIPPED);
        $this->comments = filter_var($com, FILTER_SANITIZE_STRIPPED);
        $this->mileage = filter_var($mil, FILTER_SANITIZE_STRIPPED);
        $this->liters = filter_var($ltr, FILTER_SANITIZE_STRIPPED);
        $this->trans = 0;
        $this->comment = 'New service or fill up.';
        $this->newService();
    }
    
    private function newService(){
        try {
            //$uri = "https://logbook-5a408.firebaseio.com";
            //$firebase = (new Factory)->withDatabaseUri($uri)->create();
            $sac = ServiceAccount::fromJsonFile(__DIR__.'/logbook-sac.json');
            $firebase = (new Factory)->withServiceAccount($sac)->create();
            
            $database = $firebase->getDatabase();
            $datepp = date("Y/m/d H:i:s", $this->datep);
            $newS = $database->getReference('service')->push([
                "dte" => $datepp,
                "id" => $this->datep,
                "ltr" => $this->liters,
                "mil" => $this->mileage,
                "reg" => $this->registration,
                "typ" => $this->type,
                "usr" => $this->user
            ]); 
            $this->trans = $newS->getKey();
            $newVMupdate = $database->getReference('vehicle')->push([
                "reg" => $this->registration,
                "mil" => $this->mileage,
                "dte" => $datepp,
                "tra" => $this->trans
            ]);
            $this->vid = $newVMupdate->getKey();
        } catch (Exception $ex) {
            echo '<tr><td>1 '.$ex->getMessage().'</td></tr>';
        } catch (ErrorException $cx){
            echo '<tr><td>2 '.$cx->getMessage().'</td></tr>';
        } catch (Error $err){
            echo '<tr><td>3 '.$err->getMessage().'</td></tr>';
        }
        echo '<tr><td>'.$this->trans.'</td></tr>';
        echo '<tr><td>'.$this->vid.'</td></tr>';
        echo '<tr><td>'.$this->comment.'</td></tr>';
        echo '<tr><td>'.$this->datep.'</td></tr>';
        echo '<tr><td><a href="../index.php">ExitFB1</a></td></tr>';
    }
}
?>