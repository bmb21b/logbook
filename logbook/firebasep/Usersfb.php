<?php

/*
 * Usersfb.php
 */
require '../vendor/autoload.php';

use Kreait\Firebase;
use Kreait\Firebase\Auth;
use Kreait\Firebase\Database;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

/**
 * Description of Usersfb
 *
 * @author Brian
 */
class Usersfb {
    protected $ref;
    protected $dbname = 'users';
    private $name;
    private $email;
    private $password;
    private $uid;
    private $username;
    private $hashp;
    private $emailfb;

    public function __construct() {
        $this->uid = 0;
        $this->username = "0";
        $this->name = "0";
        $this->email = "0";
        $this->password = "0";
        $this->hashp = "0";
        $this->emailfb = "0";
        $this->ref = "0";
    }
    
    public function newUser($name, $email, $passwrd){
        $this->name = filter_var($name, FILTER_SANITIZE_STRIPPED);
        $this->email = filter_var($email,FILTER_VALIDATE_EMAIL);
        $this->password = filter_var($passwrd, FILTER_SANITIZE_STRIPPED);
        $this->newUserp();
    }

    public function signIn($email, $password) {
        $this->email = filter_var($email,FILTER_VALIDATE_EMAIL);
        $this->password = filter_var($password, FILTER_SANITIZE_STRIPPED);
        $this->signInUser();
    }
    
    private function signInUser(){
        try {
            //$uri = "https://logbook-5a408.firebaseio.com";
            //$firebase = (new Factory)->withDatabaseUri($uri)->create();
            $sac = ServiceAccount::fromJsonFile(__DIR__.'/logbook-sac.json');
            $firebase = (new Factory)->withServiceAccount($sac)->create();
            
            $auth = $firebase->getAuth();
            $user = $auth->verifyPassword($this->email, $this->password);
            $this->uid = $user->uid;
            $this->username = $user->displayName;
            $this->hashp = $user->passwordHash;
            $this->emailfb = $user->email;
            echo '<tr><td>'.$this->uid.'</td></tr>';
            echo '<tr><td>'.$this->username.'</td></tr>';
            echo '<tr><td>'.$this->hashp.'</td></tr>';
            if($this->uid === 0){
                echo '<tr><td>FireBase OffLine.</td></tr>';
            } else {
               echo '<tr><td>FireBase OnLine.</td></tr>';
               echo '<tr><td><a href="../views/welcome.php">Let\'s go driving FB</a></td></tr>'; 
            }
        } catch (Exception $ex) {
            echo '<tr><td>1 '.$ex->getMessage().'</td></tr>';
        } catch (ErrorException $cx){
            echo '<tr><td>2 '.$cx->getMessage().'</td></tr>';
        } catch (Error $err){
            echo '<tr><td>3 '.$err->getMessage().'</td></tr>';
        } catch (Kreait\Firebase\Exception\Auth\InvalidPassword $exc){
            echo '<tr><td>4 '.$exc->getMessage().'</td></tr>';
        }
        echo '<tr><td><a href="../index.php">ExitFB2</a></td></tr>';
    }
    
    public function signOutUser(){
        try {
            //$uri = "https://logbook-5a408.firebaseio.com";
            //$firebase = (new Factory)->withDatabaseUri($uri)->create();
            $sac = ServiceAccount::fromJsonFile(__DIR__.'/logbook-sac.json');
            $firebase = (new Factory)->withServiceAccount($sac)->create();
            
            $firebase->getAuth()->revokeRefreshTokens($this->uid);
            
            echo '<tr><td>'.$this->uid.'</td></tr>';
        } catch (Exception $ex) {
            echo '<tr><td>1 '.$ex->getMessage().'</td></tr>';
        } catch (ErrorException $cx){
            echo '<tr><td>2 '.$cx->getMessage().'</td></tr>';
        } catch (Error $err){
            echo '<tr><td>3 '.$err->getMessage().'</td></tr>';
        } catch (Kreait\Firebase\Exception\Auth\InvalidPassword $exc){
            echo '<tr><td>4 '.$exc->getMessage().'</td></tr>';
        }
        echo '<tr><td><a href="../index.php">ExitFB2</a></td></tr>';
    }


    private function newUserp(){
        try {
            //$uri = "https://logbook-5a408.firebaseio.com";
            //$firebase = (new Factory)->withDatabaseUri($uri)->create();
            $sac = ServiceAccount::fromJsonFile(__DIR__.'/logbook-sac.json');
            $firebase = (new Factory)->withServiceAccount($sac)->create();
            
            $auth = $firebase->getAuth();
            $userProp = [
                "displayName" => $this->name,
                "email" => $this->email,
                "password" => $this->password,
                "emailVerified" => false,
                "disabled" => false
            ];
            $createdUser = $auth->createUser($userProp);
            $this->uid = $createdUser->uid;
            $this->username = $auth->getUser($this->uid);
            $auth->sendEmailVerification($this->uid);
            $database = $firebase->getDatabase();
            $datepp = date("Y/m/d H:i:s", time());
            $newUsr = $database->getReference('user')->push([
                "dte" => $datepp,
                "ema" => $this->email,
                "hsh" => "h***sh",
                "id" => $this->uid,
                "nam" => $this->name
            ]);
            $this->ref = $newUsr->getKey();
        } catch (Exception $ex) {
            echo '<tr><td>1 '.$ex->getMessage().'</td></tr>';
        } catch (ErrorException $cx){
            echo '<tr><td>2 '.$cx->getMessage().'</td></tr>';
        } catch (Error $err){
            echo '<tr><td>3 '.$err->getMessage().'</td></tr>';
        }
        echo '<tr><td>Ref# '.$this->newUserRef().'</td></tr>';
        echo '<tr><td><a href="../index.php">ExitFB1</a></td></tr>';
    }
    
    public function newUserRef(){
        return $this->ref;
    }

    public function getUid(){
        return $this->uid;
    }
    
    public function getUsername(){
        return $this->username;
    }
    
    public function getHashp() {
        return $this->hashp;
    }
    
    public function getEmail(){
        return $this->emailfb;
    }
}

?>