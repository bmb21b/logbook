<?php

/*
 * Reportjs.php
 */
$app = new Reportjs();
/**
 * Description of Reportjs
 *
 * @author Brian
 */
class Reportjs {
    var $url;
    function __construct() {
        $this->url = "logbook-5a408-export.json";
        $this->report();
    }

    private function report() {
        try {
            $data = file_get_contents($this->url);
            $arry = json_decode($data, true);
            echo '<br>';
            echo $data, "<hr>";
            echo "<H1> DB Array Size: ",sizeof($arry),"</H1><hr>";
            foreach ($arry as $key1 => $v1){
                echo $key1, "<br>";
                foreach ($v1 as $key2 => $value2) {
                   echo $key2, "<br>";
                   foreach ($value2 as $key3 => $v3){
                       echo $key3, " => ", $v3, "<br>";
                   }
                }
                echo '<hr>';
            }
            echo '<br>';
        } catch (Exception $ex) {
            echo "Ex1 ",$ex->getMessage();
        } catch (ErrorException $err){
            echo "Err2 ",$err->getMessage();
        }
    }

}
