<?php

/* 
 * index.php
 * @author Brian
 */

include 'header.php';
if(!isset($_SESSION["username"]))
{
    header("Location: ../views/login.php");
    exit();
}
?>

<?php 
    echo "<table><tr><td>".$_SESSION["userid"]."</td>";
    echo "<td>".$_SESSION["useridfb"]."</td>";
    echo "<td><a href=\"../index.php\">Log Out</a></td></tr></table>";
    $username = $_SESSION["username"]; 
?>
<hr><br>
<section id="more"></section>
<div>
    <ul>
        <li>Welcome</li>
        <li><a href="vregistration.php">Vehicle registration</a></li>
        <li><a href="vtrip.php">Trips</a></li>
        <li><a href="vservice.php">Service</a></li>
        <li><a href="vservice.php"><img src="../logbook.png"/>Fill up</a></li>
        <li><a href="../libs/report.php">Reports MySQL</a></li>
        <li><a href="../reports/Pdfreport.php">Vehicle PDF reports</a></li>
        <li><a href="../reports/Userpdf.php">User PDF reports</a></li>
        <li><a href="../reports/Trippdf.php">Trip PDF reports</a></li>
        <li><a href="../reports/Servicepdf.php">Service PDF reports</a></li>
        <li><a href="../firebasep/Reportfb.php">Reports FB online</a></li>
        <li><a href="../firebasep/Reportjs.php">Reports FB json offline</a></li>
    </ul>
</div>
<?php
echo "<br>  <br>";
include "footer.php";
?>
