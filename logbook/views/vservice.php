<?php

/* 
 * vtrip
 * @author Brian
 */

include 'header.php';
if(!isset($_SESSION["username"]))
{
    header("Location: ../views/login.php");
    exit();
}
echo "<table><tr><td>".$_SESSION["userid"]."</td>";
echo "<td>".$_SESSION["useridfb"]."</td>";
echo "<td><a href=\"../views/welcome.php\">Home</a></td>";
echo "<td><a href=\"../index.php\">Log Out</a></td></tr></table>";
?>
<?php
    $username = $_SESSION["username"];
    $td = time();
    $datepp = date("Y/m/d", $td);
?>
<hr>
<div>
    <?php
    if(isset($_REQUEST["message"]))
    {
        $msg = filter_var($_REQUEST["message"], FILTER_SANITIZE_STRIPPED);
        echo "<br>";
        echo "$msg <br>";
        echo "<br><hr><br>";
    }
    ?>
    Service or fill up <br>
</div>
<table>
    <form action='../libs/Service.php' method='POST'>  
    <tr><td>Name:</td>
        <td><input name="name" type="text" value="<?php echo $username; ?>" required="true" readonly="true" maxlength="29"/></td>
    </tr>
    <tr><td>Registration:</td>
        <td><input name="registration" type="text" value="" required="true" maxlength="29"/></td>
    </tr>
    <tr><td>Mileage:</td>
        <td><input name="mileage" type="text" value="" required="true" minlength="1" maxlength="29"/></td>
    </tr>
    <tr><td>Type:</td>
        <td><select name="ttype">
                <option value="service">Service</option>
                <option selected="" value="fillup">Fill up</option>
            </select>
        </td>
    </tr>
    <tr><td>Liters:</td>
        <td><input name="liter" type="text" value="0.0" required="true" minlength="1" maxlength="29"/></td>
    </tr>
    <tr><td>Date:</td>
        <td><input name="date" type="date" value="<?php echo $datepp; ?>" required="true" maxlength="29"/></td>
    </tr>
    <tr><td>Comments:</td>
        <td><input name="comments" type="text" value="" maxlength="99"/></td>
    </tr>
    <tr>
        <td><input type="submit" name="register" value="OK"/></td>
    </tr>
    </form>
</table>

<?php
echo "<br><br>";
include "footer.php";
?>

