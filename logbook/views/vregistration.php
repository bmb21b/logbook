<?php

/* 
 * vregistration.php
 * @author Brian
 */

include 'header.php';
if(!isset($_SESSION["username"]))
{
    header("Location: ../views/login.php");
    exit();
}
echo "<table><tr><td>".$_SESSION["userid"]."</td>";
echo "<td>".$_SESSION["useridfb"]."</td>";
echo "<td><a href=\"../views/welcome.php\">Home</a></td>";
echo "<td><a href=\"../index.php\">Log Out</a></td></tr></table>";
?>
<hr>
<div>
    <?php
    if(isset($_REQUEST["message"]))
    {
        $msg = filter_var($_REQUEST["message"], FILTER_SANITIZE_STRIPPED);
        echo "<br>";
        echo "$msg <br>";
        echo "<br><hr><br>";
    }
    ?>
</div>

<table>
    <form action='../libs/Vehicle.php' method='POST'>  
    <tr><td>Registration:</td>
        <td><input name="registration" type="text" value="" required="true" maxlength="29"/></td>
    </tr>
    <tr><td>Make:</td>
        <td><input name="make" type="text" value="" required="true" maxlength="29"/></td>
    </tr>
    <tr><td>Model:</td>
        <td><input name="model" type="text" value="" required="true" minlength="1" maxlength="29"/></td>
    </tr>
    <tr><td>Mileage:</td>
        <td><input name="mileage" type="text" value="" required="true" minlength="1" maxlength="29"/></td>
    </tr>
    <tr>
        <td><input type="submit" name="vregister" value="Register"/></td>
    </tr>
    </form>
</table>

<?php
echo "<br><br>";
include "footer.php";
?>
