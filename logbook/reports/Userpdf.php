<?php

/*
 * Userpdf.php
 * @author Brian
 */
session_start();
include '../configs/dbconfig.php';
include_once '../configs/dbconn.php';
require '../fpdf181/fpdf.php';

if(!isset($_SESSION["username"]))
{
    header("Location: ../views/login.php");
    exit();
}


/**
 * Description of Userpdf
 *
 * @author Brian
 */
class Userpdf extends FPDF {
    function Header() {
        $this->Image('../logbook.png',10,-1,70);
        $this->SetFont('Arial', 'B', 12);
        $this->Cell(80);
        $this->Cell(80,10,'Users',1,0,'C');
        $this->Ln(20);
    }
    
    function Footer() {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $this->Cell(0, 10, 'Page '.$this->PageNo().'/{nb}', 0,0,'C');
    }
}

$mysqli = connDB();
$query = "SELECT * FROM user";
$smt = $mysqli->prepare($query);
$smt->execute();
$rs = $smt->get_result();
$fc = $rs->field_count;
$nc = $rs->num_rows;

$pdf = new Userpdf('L','mm','Legal');
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetFont('Times', '', 12);
$w = array(25, 40, 60, 155, 55);
$hd = array("ID", "Name", "Email", "HashCode", "Date");
$c = 0;
foreach ($hd as $l){
    $pdf->Cell($w[$c], 15, $l, 1,0,'C');
    $c++;
}
$pdf->Ln();

for ($j = 0; $j < $nc; $j++ )
{
    $rsa = $rs->fetch_array();
    for ($i = 0; $i < $fc; $i++)
    {
        $pdf->Cell($w[$i], 10, $rsa[$i],1,0,'L');
    }
    $pdf->Ln();
}
$smt->close();
$mysqli->close();
$pdf->Output();

?>