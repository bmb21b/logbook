<?php

/*
 * Pdfreport.php
 * @author Brian
 */
session_start();
include '../configs/dbconfig.php';
include_once '../configs/dbconn.php';
require '../fpdf181/fpdf.php';

if(!isset($_SESSION["username"]))
{
    header("Location: ../views/login.php");
    exit();
}

/**
 * Description of Pdfreport
 *
 * @author Brian
 */
class Pdfreport extends FPDF {
    
    function Header() {
        $this->Image('../logbook.png',10,-1,70);
        $this->SetFont('Arial', 'B', 12);
        $this->Cell(80);
        $this->Cell(80,10,'Vehicles',1,0,'C');
        $this->Ln(20);
    }
    
    function Footer() {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $this->Cell(0, 10, 'Page '.$this->PageNo().'/{nb}', 0,0,'C');
    }
}

$mysqli = connDB();
$query = "SELECT * FROM vehicle";
$smt = $mysqli->prepare($query);
$smt->execute();
$rs = $smt->get_result();
$fc = $rs->field_count;
$nc = $rs->num_rows;

$pdf = new Pdfreport('L','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetFont('Times', '', 12);
$hdt = array("ID", "Registration", "Make", "Model", "Mileage");
foreach ($hdt as $l){
    $pdf->Cell(40, 15, $l, 1, 0, "C");
}
$pdf->Ln();

for ($j = 0; $j < $nc; $j++ )
{
    $rsa = $rs->fetch_array();
    for ($i = 0; $i < $fc; $i++)
    {
        $pdf->Cell(40, 10, $rsa[$i],1);
    }
    $pdf->Ln();
}
$smt->close();
$mysqli->close();
$pdf->Output();
?>
