#///////////////////////////////////
#/* 
# * logbook.sql
# */
#/**
# * Author:  Brian
# * Created: 16 Jan 2019
# */
#////////////////////////////////////////

CREATE DATABASE LOGBOOK;

USE LOGBOOK;

CREATE TABLE USER (
id int NOT NULL auto_increment,
name varchar (30) NOT NULL,
email varchar (30) NOT NULL,
password varchar (100) NOT NULL,
date datetime NOT NULL default '0000/00/00 00:00:00',
PRIMARY KEY (id)
);

CREATE TABLE VEHICLE (
id int NOT NULL auto_increment,
registration varchar (30) NOT NULL,
make varchar (30) NOT NULL,
model varchar (30) NOT NULL,
mileage float (10,2) NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE TRIP (
id int NOT NULL auto_increment,
registration varchar (30) NOT NULL,
user varchar (30) NOT NULL,
date datetime NOT NULL default '0000/00/00 00:00:00',
business boolean,
personal boolean,
comment text,
PRIMARY KEY (id)
);

CREATE TABLE SERVICE (
id int NOT NULL auto_increment,
registration varchar (30) NOT NULL,
user varchar (30) NOT NULL,
date datetime NOT NULL default '0000/00/00 00:00:00',
service boolean,
petrol boolean,
liter float (6,2),
mileage float (10,2) NOT NULL,
comment text NOT NULL,
PRIMARY KEY (id)
);

GRANT ALL ON logbook.* to logbv@localhost identified by 'log2pw';

\s